# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
# def transmogrify(string, options = {})
#   defaults = {times:1, upcase: false, reverse:false }.merge(options)
#   if defaults[:upcase] && defaults[:reverse]
#     result = defaults[:times].times do |str| puts string.upcase.reverse end
#   elsif defaults[:upcase]
#     reuslt = defaults[:times].times do |str| puts string.upcase end
#   elsif defaults[:reverse]
#     result = defaults[:times].times do |str| puts string.reverse end
#   else
#     result = defaults[:times].times do |str| puts string end
#   end
#   result
# end

def transmogrify(string, options = {})
  result = []
  defaults = {times:1, upcase: false, reverse:false }.merge(options)
  if defaults[:upcase] && defaults[:reverse]
    defaults[:times].times do |str|  result << string.upcase.reverse end
  elsif defaults[:upcase]
    defaults[:times].times do |str| result << string.upcase end
  elsif defaults[:reverse]
    defaults[:times].times do |str| result << string.reverse end
  else
    defaults[:times].times do |str| result << string end
  end
  result.join
end
